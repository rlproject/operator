# README #

Document steps that necessary to get 'gui.Operator' up and running.

### What is this repository for? ###

* gui.Operator is an 'outer shell' that should run AI games that supports "SessionProxy" API.

### How do I get set up? ###

* First, clone the repository to your workspace
* As a part of this repository, there are 2 external libraries crucial to the operator functioning:
 1) jcommon-1.0.23.jar
 2) jfreechart-1.0.19.jar
* The two packages should be added manually to the project as 'external libraries.
* As a 'main argument', the operator expects to get an absolute path to the game directory that should
	have all compiled classes and the RL similarities table file ("table.ser").
* If all above prequisities are met, the operator can be run with no problems.
