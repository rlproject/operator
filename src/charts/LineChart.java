package charts;

import gui.Operator;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.block.LineBorder;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardXYItemLabelGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYErrorRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.chart.title.Title;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.TextAnchor;
import statistics.DataHolder;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.geom.Ellipse2D;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Line chart frame
 */
class LineChart extends ApplicationFrame {
    /**
     * constructor
     * @param applicationTitle - chart title
     * @param chartDataType - type of chart
     * @param dataSets - dataet to display
     */
    public LineChart(String applicationTitle, String chartDataType, List<IntervalXYDataset> dataSets) {
        super(applicationTitle);

        //create chart
        JFreeChart lineChart = ChartFactory.createXYLineChart(
                getChartTitle(chartDataType),
                getXAxisName(chartDataType),
                getYAxisName(chartDataType),
                null);

        //create subtitle with the domain type
        lineChart.setSubtitles(getSubtitle(chartDataType));
        //customize chart properties
        customizeChart(lineChart, dataSets);
        //add legend
        createLegend(lineChart, dataSets.get(0));

        //create panel
        ChartPanel chartPanel = new ChartPanel(lineChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(Toolkit.getDefaultToolkit().getScreenSize().width / 2,
                Toolkit.getDefaultToolkit().getScreenSize().height / 2));
        setContentPane(chartPanel);

        try {
            int width = 1000;
            int height = 600;
            //save every chart as a png file
            ChartUtilities.saveChartAsPNG(
                    new File(Operator.dirName + "/" + chartDataType + ".png"), lineChart, width, height);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * create subtitle with the domain name
     * @param chartDataType - type of chart
     * @return domain subtitle
     */
    private List getSubtitle(String chartDataType) {
        List<Title> subtitles = new LinkedList<>();
        if(chartDataType.equals(DataHolder.MOVES)) {
            TextTitle tt = new TextTitle("Domain: Chase game");
            subtitles.add(tt);

        } else {
            TextTitle tt = new TextTitle("Domain: Super Mario game");
            subtitles.add(tt);
        }
        return subtitles;
    }

    /**
     * get Y axis name
     * @param chartDataType charrt type
     * @return Y axis name
     */
    private String getYAxisName(String chartDataType) {
        if (chartDataType.equals(DataHolder.MOVES)) {
            return "Average Moves (lower is better)";
        } else if (chartDataType.equals(DataHolder.KILLS)) {
            return "Average Kill count (higher is better)";
        } else if (chartDataType.equals(DataHolder.WIN)) {
            return "Average win percentage (higher is better)";
        }else if (chartDataType.equals(DataHolder.PROGRESSION)) {
            return "Average Progression (higher is better)";
        } else {
            return null;
        }
    }

    /**
     * get X axis name
     * @param chartDataType - chart type
     * @return X axis name
     */
    private String getXAxisName(String chartDataType) {
        return "Batch No.";
    }

    /**
     * get chart title
     * @param chartDataType - chart type
     * @return - chart title
     */
    private String getChartTitle(String chartDataType) {
        String chartTitleName = "Comparing Learning curve of QLearning and SLearning: ";
        if(chartDataType.equals(DataHolder.KILLS)) {
            chartTitleName = chartTitleName + "Kills";
        } else if(chartDataType.equals(DataHolder.PROGRESSION)) {
            chartTitleName = chartTitleName + "Progression";
        } else if(chartDataType.equals(DataHolder.WIN)) {
            chartTitleName = chartTitleName + "Wins";
        } else if(chartDataType.equals(DataHolder.MOVES)) {
            chartTitleName = chartTitleName + "Moves";
        }
        return chartTitleName;
    }

    /**
     * customize chart propertis:
     * - line colors
     * - chart data type (double)
     * - shape of lines
     *
     * @param chart - chart to apply customization
     * @param dataSets - dataset to customize
     */
    private void customizeChart(JFreeChart chart, List<IntervalXYDataset> dataSets) {
        IntervalXYDataset dataset1 = dataSets.get(0);
        IntervalXYDataset dataset2 = dataSets.get(1);

        //create circle shape
        Shape circle = new Ellipse2D.Double(-2, -2, 4, 4);
        //create bold line
        BasicStroke boldLine = new BasicStroke(2.5f);
        //create dashed line
        BasicStroke dashedLine = new BasicStroke(
                1.0f,
                BasicStroke.CAP_ROUND,
                BasicStroke.JOIN_ROUND,
                1.0f,
                new float[]{6.0f, 6.0f}, 0.0f);
        //create error stroke line
        BasicStroke errorStrokeLine = new BasicStroke(0.5f);

        XYErrorRenderer r1 = new XYErrorRenderer();
        r1.setBaseLinesVisible(true);
        r1.setErrorStroke(errorStrokeLine);
        r1.setErrorPaint(Color.BLACK);
        r1.setDrawXError(false);
        r1.setDrawYError(true);
        r1.setCapLength(3);

        r1.setSeriesPaint(0, Color.BLUE);
        r1.setSeriesPaint(1, Color.BLUE);
        r1.setSeriesPaint(2, Color.BLUE);
        r1.setSeriesShape(0, circle);
        r1.setSeriesStroke(0, boldLine);
        r1.setSeriesStroke(1, dashedLine);
        r1.setSeriesStroke(2, dashedLine);
        r1.setSeriesShapesVisible(0, true);
        r1.setSeriesShapesVisible(1, false);
        r1.setSeriesShapesVisible(2, false);

        r1.setBaseItemLabelGenerator(new StandardXYItemLabelGenerator());
        r1.setSeriesItemLabelsVisible(0, Boolean.TRUE);
        r1.setSeriesItemLabelsVisible(1, Boolean.FALSE);
        r1.setSeriesItemLabelsVisible(2, Boolean.FALSE);
        r1.setSeriesPositiveItemLabelPosition(0,
                new ItemLabelPosition());

        XYErrorRenderer r2 = new XYErrorRenderer();
        r2.setBaseLinesVisible(true);
        r2.setErrorStroke(errorStrokeLine);
        r2.setErrorPaint(Color.BLACK);
        r2.setDrawXError(false);
        r2.setDrawYError(true);
        r2.setCapLength(3);

        r2.setSeriesPaint(0, Color.RED);
        r2.setSeriesPaint(1, Color.RED);
        r2.setSeriesPaint(2, Color.RED);
        r2.setSeriesShape(0, circle);
        r2.setSeriesStroke(0, boldLine);
        r2.setSeriesStroke(1, dashedLine);
        r2.setSeriesStroke(2, dashedLine);
        r2.setSeriesShapesVisible(0, true);
        r2.setSeriesShapesVisible(1, false);
        r2.setSeriesShapesVisible(2, false);
        r2.setSeriesItemLabelsVisible(1, Boolean.TRUE);
        r2.setBaseItemLabelsVisible(true);

        r2.setBaseItemLabelGenerator(new StandardXYItemLabelGenerator());
        r2.setSeriesItemLabelsVisible(0, Boolean.TRUE);
        r2.setSeriesItemLabelsVisible(1, Boolean.FALSE);
        r2.setSeriesItemLabelsVisible(2, Boolean.FALSE);
        r2.setSeriesPositiveItemLabelPosition(0,
                new ItemLabelPosition(
                        ItemLabelAnchor.OUTSIDE8, TextAnchor.TOP_CENTER));

        XYPlot plot = (XYPlot) chart.getPlot();

        plot.setDataset(0, dataset1);
        plot.setRenderer(0, r1);

        plot.setDataset(1, dataset2);
        plot.setRenderer(1, r2);

        plot.getDomainAxis().setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        plot.getDomainAxis().setRange(0, ChartCreator.X_AXIS_RANGE + 1);

        plot.getRangeAxis().setRange(ChartCreator.MIN_CHART_VALUE - 5, ChartCreator.MAX_CHART_VALUE + 5);

        plot.setBackgroundPaint(new Color(0xFF, 0xFF, 0xFF));
        plot.setDomainGridlinePaint(new Color(202, 191, 191));
        plot.setRangeGridlinePaint(new Color(202, 191, 191));
    }

    private void createLegend(JFreeChart chart, IntervalXYDataset dataset) {
        LegendTitle legend = new LegendTitle(chart.getPlot());
        legend.setMargin(new RectangleInsets(1.0D, 1.0D, 1.0D, 1.0D));
        legend.setFrame(new LineBorder());
        legend.setBackgroundPaint(Color.white);
        legend.setPosition(RectangleEdge.BOTTOM);
        chart.addSubtitle(legend);
        legend.addChangeListener(chart);
    }

    /**
     * st 'on close' behavior
     * @param event
     */
    public void windowClosing(WindowEvent event) {
        if(event.getWindow() == this) {
            this.dispose();
        }

    }
}