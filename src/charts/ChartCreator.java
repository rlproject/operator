package charts;

import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.YIntervalSeries;
import org.jfree.data.xy.YIntervalSeriesCollection;
import org.jfree.ui.RefineryUtilities;
import session.SessionProxy;
import statistics.DataHolder;
import statistics.StatisticsHolder;

import javax.swing.*;
import java.util.*;

/**
 * ChartCreator class
 * Creates  statistics Charts, based on a given database.
 */
public class ChartCreator {
    public static double MAX_CHART_VALUE = Double.MIN_VALUE;
    public static double MIN_CHART_VALUE = Double.MAX_VALUE;
    public static int X_AXIS_RANGE = 10;

    /**
     * creates 'learning curve' chart
     * @param statHolder - statistics data base
     * @param chartDataType - type of chart (objective) to display (moves/wins/kills/progression)
     */
    public static void createLearningCurveChart(StatisticsHolder statHolder, String chartDataType) {
        //set chart window title
        String windowTitleName = "QLearning Vs SLearning";
        //create chart
        LineChart chart = new LineChart(
                windowTitleName,
                chartDataType,
                createLineChartDatasetsWithErrors(statHolder, chartDataType));
        chart.pack();
        chart.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        RefineryUtilities.centerFrameOnScreen(chart);
        //display chart
        chart.setVisible(true);
    }

    /**
     * creates all chart datasets with error bars to display
     * @param statHolder - statistics data base
     * @param chartDataType - type of chart (objective) to display (moves/wins/kills/progression)
     * @return datasets to display
     */
    private static List<IntervalXYDataset> createLineChartDatasetsWithErrors(
            StatisticsHolder statHolder, String chartDataType) {

        List<IntervalXYDataset> datasets = new LinkedList<>();
        datasets.add(createDataSetWithErrors(statHolder, SessionProxy.TYPE_REGULAR, chartDataType));
        datasets.add(createDataSetWithErrors(statHolder, SessionProxy.TYPE_SIMILARITY, chartDataType));
        return datasets;
    }

    /**
     * create and customize single dataset for thee chart
     * @param statHolder - database
     * @param agentType - type off agent (regular/similarities)
     * @param chartDataType - type of chart
     * @return dataset
     */
    private static IntervalXYDataset createDataSetWithErrors(
            StatisticsHolder statHolder, int agentType, String chartDataType) {

        YIntervalSeriesCollection collection = new YIntervalSeriesCollection();

        //set labels
        String avgMovesLabel = agentType == SessionProxy.TYPE_SIMILARITY ? "S Average " : "Q Average ";
        avgMovesLabel = avgMovesLabel + chartDataType;
        String upperBoundLabel = agentType == SessionProxy.TYPE_SIMILARITY ? "S Upper Bound" : "Q Upper Bound";
        String lowerBoundLabel = agentType == SessionProxy.TYPE_SIMILARITY ? "S Lower Bound" : "Q Lower Bound";

        //create 3 lines per database: learning curve, upper bound and lower bound
        final YIntervalSeries s1 = new YIntervalSeries(avgMovesLabel);
        final YIntervalSeries s2 = new YIntervalSeries(upperBoundLabel);
        final YIntervalSeries s3 = new YIntervalSeries(lowerBoundLabel);

        //get agent's database
        DataHolder agent = statHolder.getAgent(agentType);

        //extract raw data
        List<Double> learningCurveData = agent.getCurve(chartDataType);
        List<Double> learningCurveErrors = agent.getError(chartDataType);
        double upperBound = agent.getUpperBound(chartDataType);
        double lowerBound = agent.getLowerBound(chartDataType);

        //set max/min values of chart
        MAX_CHART_VALUE = upperBound > MAX_CHART_VALUE ? upperBound : MAX_CHART_VALUE;
        MIN_CHART_VALUE = lowerBound < MIN_CHART_VALUE ? lowerBound : MIN_CHART_VALUE;
        X_AXIS_RANGE = learningCurveData.size();

        int valCounter = 1;
        //create chart-compatible learning curve, upper/lower bound
        for (Double val : learningCurveData) {
            double error = learningCurveErrors.get(valCounter - 1);
            s1.add(valCounter, val, val - error, val + error);
            s2.add(valCounter, upperBound, 0, 0);
            s3.add(valCounter, lowerBound, 0, 0);
            valCounter++;
        }

        s2.add(0, upperBound, 0, 0);
        s2.add(learningCurveData.size() + 1, upperBound, 0, 0);
        s3.add(0, lowerBound, 0, 0);
        s3.add(learningCurveData.size() + 1, lowerBound, 0, 0);

        collection.addSeries(s1);
        collection.addSeries(s2);
        collection.addSeries(s3);
        //return collection
        return collection;
    }
}
