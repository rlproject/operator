package gui;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;
import java.io.IOException;
import java.io.OutputStream;

/**
 * This class extends from OutputStream to redirect output to a JTextPane
 */
public class CustomOutputStream extends OutputStream {
    private JTextPane textPane;
    StyledDocument doc;

    public CustomOutputStream(JTextPane tp) {
        textPane = tp;
        doc = tp.getStyledDocument();
    }

    @Override
    public void write(int b) throws IOException {
        // redirects data to the text pane
        try {
            doc.insertString(doc.getLength(), String.valueOf((char)b), null);
            textPane.setCaretPosition(doc.getLength());
        } catch (BadLocationException e) {
            e.printStackTrace();
            throw new IOException();
        }
    }
}