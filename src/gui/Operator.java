package gui;

import charts.ChartCreator;
import session.ChaseSessionRunner;
import session.MarioSessionRunner;
import session.SessionProxy;
import session.SessionRunner;
import statistics.DataHolder;
import statistics.StatisticsHolder;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * gui.Operator class:
 * <p>
 * Should get (as a main argument) a path to the compiled folder of
 * the game to be run.
 * <p>
 * Runs the game with 2 agents (Q Learning agent, Similarities Agent),
 * and draws statistics on a chart
 */
public class Operator {
    public final static String MARIO = "mario";
    public final static String CHASE = "chase";
    private long time;

    //domain name
    private String domain;
    //session runner - the general proxy object that operate the game
    private SessionRunner sessionRunner;
    //hold the results in statistics "holder object"
    private final StatisticsHolder statisticsHolder = new StatisticsHolder();

    /**
     * The text pane which is used for displaying logging information.
     */
    public static JTextPane textPane;
    public static String dirName = null;
    private PrintStream printStream;
    private PrintStream standardOut;

    /**
     * Constructor
     */
    public Operator() {
        //text pane to redirect all printings.
        textPane = new JTextPane();
        textPane.setEditable(false);
        textPane.setFont(new Font("Arial", Font.PLAIN, 35));
        StyledDocument doc = textPane.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);

        //new output stream
        printStream = new PrintStream(new CustomOutputStream(textPane));

        // keeps reference of standard output stream
        standardOut = System.out;

        // re-assigns standard output stream and error output stream
        System.setOut(printStream);
    }

    /**
     * load the game using *path* into the session runner
     *
     * @param path - path to the game folder
     * @param tablePath - path to the similarities table
     */
    public void loadGame(String path, String tablePath, String dom) {
        System.out.println("-------- Loading Game --------");
        domain = dom;
        if(domain.equals(CHASE)) {
            sessionRunner = new ChaseSessionRunner(path, tablePath);
        } else {
            sessionRunner = new MarioSessionRunner(path, tablePath);
        }
        System.out.println("---- successfully  loaded ----");
    }

    /**
     * Compare between the two agents
     */
    public void compareAgents(int numOfTrains, int numOfTests, int sizeOfBatch) {
        time = System.currentTimeMillis();
        //print date
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        System.out.println("\n" + dateFormat.format(date) + "\n");
        //print setup
        System.out.println("[ Number of Trains: " + numOfTrains
                + ",\tNumber of Tests: " + numOfTests
                + ",\tSize of Batch: " + sizeOfBatch
                + " ]");
        System.out.println();
        System.out.println("------------------------------");
        System.out.println("------- Compare Agents -------");
        System.out.println("------------------------------");
        System.out.println();
        //run Q agent and save statistics in stat holder
        System.out.println("******************************");
        System.out.println("*******  Run Q Agent:  *******");
        System.out.println("******************************");
        System.out.println();
        runAgent(SessionProxy.TYPE_REGULAR, numOfTrains, numOfTests, sizeOfBatch);
        //run S agent and save statistics in stat holder
        System.out.println("******************************");
        System.out.println("*******  Run S Agent:  *******");
        System.out.println("******************************");
        System.out.println();
        runAgent(SessionProxy.TYPE_SIMILARITY, numOfTrains, numOfTests, sizeOfBatch);
        saveOutput();
        if(domain.equals(CHASE)) {
            ChartCreator.createLearningCurveChart(statisticsHolder, DataHolder.MOVES);
        } else {
            ChartCreator.createLearningCurveChart(statisticsHolder, DataHolder.KILLS);
            ChartCreator.createLearningCurveChart(statisticsHolder, DataHolder.PROGRESSION);
            ChartCreator.createLearningCurveChart(statisticsHolder, DataHolder.WIN);
        }
    }

    /**
     * run single agent and get statistics
     * @param type - agent's type
     * @param numOfTrains - number of trains
     * @param numOfTests - number of test
     */
    private void runAgent(int type, int numOfTrains, int numOfTests, int sizeOfBatch) {
        DataHolder holder = sessionRunner.runWithStatistics(type, numOfTrains, numOfTests, sizeOfBatch);
        statisticsHolder.setAgent(holder, type);
        System.out.println();
    }

    /**
     * save text output to 'out.txt' file inside 'operator_out' directory
     */
    public void saveOutput() {
        //print elapsed time
        String format = String.format("%%0%dd", 2);
        long elapsedTime = (System.currentTimeMillis() - time) / 1000;
        String seconds = String.format(format, elapsedTime % 60);
        String minutes = String.format(format, (elapsedTime % 3600) / 60);
        String hours = String.format(format, elapsedTime / 3600);
        System.out.println("elapsed time: " + hours + ":" + minutes + ":" + seconds);

        System.out.println();
        System.out.println("*****************");
        System.out.println();
        System.out.println("Saving Output");
        System.out.println();
        System.out.println("*****************");

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH.mm.ss");
        Date date = new Date();
        dirName = dateFormat.format(date);
        File outDir = new File(dirName);
        outDir.mkdir();

        String output = textPane.getText();
        try {
            PrintWriter out = new PrintWriter(dirName + "/out.txt");
            out.println(output);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
