package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * MainWindow class.
 * Main window frame.
 * In this frame the user should set:
 * - domain type (Mario/Chase)
 * - base directory path
 * - similarities table path
 */
public class MainWindow extends JFrame {
    //frame's components
    JPanel mainPanel;
    JLabel wrongInputLabel;
    JButton continueButton;
    JPanel chooseDomainPanel;
    JLabel chooseDomainLabel;
    JComboBox<String> domainCB;
    public static Font font = new Font("Segoe UI Light",Font.BOLD, 12);

    JPanel chooseLocationPanel;
    JLabel chooseLocationLabel;
    JTextField chooseLocationTextField = new JTextField();
    JButton chooseLocationButton;
    JFileChooser locationChooser;

    JPanel chooseTableLocationPanel;
    JLabel chooseTableLocationLabel;
    JTextField chooseTableLocationTextField = new JTextField();
    JButton chooseTableLocationButton;
    JFileChooser tableLocationChooser;

    /**
     * Constructor
     * @param title - main window title
     */
    public MainWindow(String title) {
        super(title);
        //init components
        init();
    }

    /**
     * initialize frame
     */
    private void init() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.setBackground(Color.WHITE);

        //add components
        initComponents();
        mainPanel.add(chooseLocationPanel);
        mainPanel.add(Box.createRigidArea(new Dimension(300, 100)));
        mainPanel.add(chooseTableLocationPanel);
        mainPanel.add(Box.createRigidArea(new Dimension(300, 100)));
        mainPanel.add(chooseDomainPanel);
        mainPanel.add(Box.createRigidArea(new Dimension(300, 50)));
        mainPanel.add(wrongInputLabel);
        mainPanel.add(Box.createRigidArea(new Dimension(300, 10)));
        mainPanel.add(continueButton);

        setLayout(new GridBagLayout());
        add(mainPanel);
        setSize(700, 700);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setBackground(Color.WHITE);
        setResizable(false);
    }

    /**
     * initialize frame's internal components
     */
    private void initComponents() {
        Font globalFont;
        /**
         * base dir chooser
         */
        chooseLocationPanel = new JPanel();
        chooseLocationPanel.setLayout(new BoxLayout(chooseLocationPanel, BoxLayout.Y_AXIS));
        chooseLocationPanel.setBackground(Color.WHITE);

        JPanel chooseDirPanel = new JPanel();
        chooseDirPanel.setLayout(new BoxLayout(chooseDirPanel, BoxLayout.X_AXIS));

        //set label
        chooseLocationLabel = new JLabel("Insert Domain Location:");
        globalFont = new Font(font.getName(), Font.PLAIN, chooseLocationLabel.getFont().getSize() + 5);
        chooseLocationLabel.setFont(globalFont);
        chooseLocationLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        //set path text field
        chooseLocationTextField.setMinimumSize(new Dimension(300, chooseLocationTextField.getSize().height));
//        chooseLocationTextField.setText("C:\\Users\\yinon\\IdeaProjects\\chase\\out\\production\\chase");

        //set "browse..." button
        chooseLocationButton = new JButton();
        chooseLocationButton.setText("Browse...");
        globalFont = new Font(font.getName(), Font.PLAIN, chooseLocationButton.getFont().getSize());
        chooseLocationButton.setFont(globalFont);
        chooseLocationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                locationChooser = new JFileChooser();
                locationChooser.setCurrentDirectory(new java.io.File("."));
                locationChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                locationChooser.setAcceptAllFileFilterUsed(false);
                if (locationChooser.showOpenDialog(MainWindow.this) == JFileChooser.APPROVE_OPTION) {
                    chooseLocationTextField.setText(locationChooser.getSelectedFile().getAbsolutePath());
                }
            }
        });

        chooseDirPanel.add(chooseLocationTextField);
        chooseDirPanel.add(chooseLocationButton);

        chooseLocationPanel.add(chooseLocationLabel);
        chooseLocationPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        chooseLocationPanel.add(chooseDirPanel);

        /**
         * table.ser chooser
         */
        chooseTableLocationPanel = new JPanel();
        chooseTableLocationPanel.setLayout(new BoxLayout(chooseTableLocationPanel, BoxLayout.Y_AXIS));
        chooseTableLocationPanel.setBackground(Color.WHITE);

        JPanel chooseTablePanel = new JPanel();
        chooseTablePanel.setLayout(new BoxLayout(chooseTablePanel, BoxLayout.X_AXIS));

        //set label
        chooseTableLocationLabel = new JLabel("Insert Table Location:");
        globalFont = new Font(font.getName(), Font.PLAIN, chooseTableLocationLabel.getFont().getSize() + 5);
        chooseTableLocationLabel.setFont(globalFont);
        chooseTableLocationLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        //set path text field
        chooseTableLocationTextField.setMinimumSize(new Dimension(300, chooseTableLocationTextField.getSize().height));
//        chooseTableLocationTextField.setText("C:\\Users\\yinon\\IdeaProjects\\chase\\table.ser");

        //set "browse..." button
        chooseTableLocationButton = new JButton();
        chooseTableLocationButton.setText("Browse...");
        globalFont = new Font(font.getName(), Font.PLAIN, chooseTableLocationButton.getFont().getSize());
        chooseTableLocationButton.setFont(globalFont);
        chooseTableLocationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tableLocationChooser = new JFileChooser();
                tableLocationChooser.setCurrentDirectory(new java.io.File("."));
                tableLocationChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                tableLocationChooser.setAcceptAllFileFilterUsed(true);
                if (tableLocationChooser.showOpenDialog(MainWindow.this) == JFileChooser.APPROVE_OPTION) {
                    chooseTableLocationTextField.setText(tableLocationChooser.getSelectedFile().getAbsolutePath());
                }
            }
        });

        chooseTablePanel.add(chooseTableLocationTextField);
        chooseTablePanel.add(chooseTableLocationButton);

        chooseTableLocationPanel.add(chooseTableLocationLabel);
        chooseTableLocationPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        chooseTableLocationPanel.add(chooseTablePanel);

        /**
         * domain chooser combo box
         */
        chooseDomainPanel = new JPanel();
        chooseDomainPanel.setLayout(new BoxLayout(chooseDomainPanel, BoxLayout.Y_AXIS));
        chooseDomainPanel.setBackground(Color.WHITE);

        //set label
        chooseDomainLabel = new JLabel("Choose Domain:");
        globalFont = new Font(font.getName(), Font.PLAIN, chooseDomainLabel.getFont().getSize() + 5);
        chooseDomainLabel.setFont(globalFont);
        chooseDomainLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        //set combo box
        String[] domains = {Operator.CHASE, Operator.MARIO};
        domainCB = new JComboBox<String>(domains);
        ((JLabel)domainCB.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
        globalFont = new Font(font.getName(), Font.PLAIN, domainCB.getFont().getSize());
        domainCB.setFont(globalFont);

        chooseDomainPanel.add(chooseDomainLabel);
        chooseDomainPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        chooseDomainPanel.add(domainCB);


        wrongInputLabel = new JLabel("");
        wrongInputLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        wrongInputLabel.setText("Please fill both fields with relevant paths");
        globalFont = new Font(font.getName(), Font.PLAIN, wrongInputLabel.getFont().getSize());
        wrongInputLabel.setFont(globalFont);

        continueButton = new JButton("Continue");
        globalFont = new Font(font.getName(), Font.PLAIN, continueButton.getFont().getSize());
        continueButton.setFont(globalFont);
        continueButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        continueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(chooseLocationTextField.getText().isEmpty()
                        || chooseTableLocationTextField.getText().isEmpty()) {
                    wrongInputLabel.setForeground(Color.RED);
                } else {
                    String[] args = {chooseLocationTextField.getText(),
                            chooseTableLocationTextField.getText(),
                            (String) domainCB.getSelectedItem()};
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                            } catch (ClassNotFoundException | InstantiationException
                                    | IllegalAccessException | UnsupportedLookAndFeelException e) {
                                e.printStackTrace();
                            }
                            Operator opr = new Operator();
                            opr.loadGame(args[0], args[1], args[2]);
                            JFrame agentComparatorFrame = new AgentComparatorFrame(opr, args[0], args[1], "" + domainCB.getSelectedIndex());
                            agentComparatorFrame.setVisible(true);
                        }
                    });
                    MainWindow.this.dispose();
                }
            }
        });
    }

    /**
     * set base directory and table paths
     * @param location - base directory path
     * @param table - similarities table path
     */
    public void setTextFields(String location, String table, String index) {
        chooseLocationTextField.setText(location);
        chooseTableLocationTextField.setText(table);
        domainCB.setSelectedIndex(Integer.parseInt(index));
    }

    /**
     * main function. starts the whole operator process.
     * @param args
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException
                        | IllegalAccessException | UnsupportedLookAndFeelException e) {
                    e.printStackTrace();
                }
                MainWindow mainWindow = new MainWindow("Operator");
                if (args.length == 3) {
                    mainWindow.setTextFields(args[0], args[1], args[2]);
                }
                mainWindow.setVisible(true);
            }
        });
    }
}
