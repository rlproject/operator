package gui;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * AgentComparatorFrame class
 * Agent Comparator Frame.
 * In this frame the user should set train and test number
 * and run comparison.
 * Output appears in the text area.
 * A statistics graph pops up after comparison ends.
 */
public class AgentComparatorFrame extends JFrame {
    //object that runs the agents comparison
    Operator operator;
    public static int NUM_OF_TRAINS;
    public static int NUM_OF_TESTS;
    public static int NUM_OF_BATCHES;
    private static int MODE_SET_NUM_OF_TRAINS = 0;
    private static int MODE_SET_NUM_OF_TESTS = 1;
    private static int MODE_SET_NUM_OF_BATCHES = 2;

    //frame's components
    private JButton backToMenu;
    private JButton trainTestButton;
    private JTextField timesToTrainTestTextField;
    private JLabel trainTestLabel;

    private int setMode = MODE_SET_NUM_OF_TRAINS;
    private String locationPath;
    private String tablePath;
    private String domain;

    public JPopupMenu popup;
    JMenuItem pauseItem;
    JMenuItem continueItem;
    JMenuItem exitItem;
    Thread comparisonThread;

    /**
     * constructor
     * @param opr - operator object
     * @param location - base directory location path
     * @param table - similarities table path
     */
    public AgentComparatorFrame(Operator opr, String location, String table, String dom) {
        //set title
        super("Operator");
        //save properties
        operator = opr;
        locationPath = location;
        tablePath = table;
        domain = dom;
        //init frame components
        init();
    }

    /**
     * initialize frame's components
     */
    private void init() {
        initPopupMenu();
        Font globalFont;

        trainTestLabel = new JLabel("Set Number Of Trains: ");
        globalFont = new Font(MainWindow.font.getName(), Font.PLAIN, trainTestLabel.getFont().getSize());
        trainTestLabel.setFont(globalFont);

        timesToTrainTestTextField = new JTextField();
        timesToTrainTestTextField.setMinimumSize(new Dimension(20, timesToTrainTestTextField.getSize().height));

        trainTestButton = new JButton("Continue");
        globalFont = new Font(MainWindow.font.getName(), Font.PLAIN, trainTestButton.getFont().getSize());
        trainTestButton.setFont(globalFont);
        trainTestButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (setMode == MODE_SET_NUM_OF_TRAINS) {
                    trainTestButton.setEnabled(false);
                    String input = timesToTrainTestTextField.getText();
                    timesToTrainTestTextField.setText("");
                    timesToTrainTestTextField.setEditable(false);
                    trainTestLabel.setText("Set Number Of Tests: ");
                    int times = input.isEmpty() ? 1 : Integer.parseInt(input);
                    NUM_OF_TRAINS = times;
                    trainTestButton.setEnabled(true);
                    timesToTrainTestTextField.setEditable(true);
                    setMode = MODE_SET_NUM_OF_TESTS;
                } else if (setMode == MODE_SET_NUM_OF_TESTS) {
                    trainTestButton.setEnabled(false);
                    trainTestButton.setText("Begin Comparison");
                    String input = timesToTrainTestTextField.getText();
                    timesToTrainTestTextField.setText("");
                    timesToTrainTestTextField.setEditable(false);
                    trainTestLabel.setText("Set Number Of Batches: ");
                    int times = input.isEmpty() ? 1 : Integer.parseInt(input);
                    NUM_OF_TESTS = times;
                    trainTestButton.setEnabled(true);
                    timesToTrainTestTextField.setEditable(true);
                    setMode = MODE_SET_NUM_OF_BATCHES;
                } else if (setMode == MODE_SET_NUM_OF_BATCHES){
                    trainTestButton.setEnabled(false);
                    backToMenu.setEnabled(false);
                    String input = timesToTrainTestTextField.getText();
                    timesToTrainTestTextField.setEditable(false);
                    int times = input.isEmpty() ? 1 : Integer.parseInt(input);
                    NUM_OF_BATCHES = times;
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            pauseItem.setEnabled(true);
                            operator.compareAgents(NUM_OF_TRAINS, NUM_OF_TESTS, NUM_OF_BATCHES);
                            backToMenu.setEnabled(true);
                            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                        }
                    };
                    comparisonThread = new Thread(runnable);
                    comparisonThread.start();
                }
            }
        });

        backToMenu = new JButton("Back to Menu");
        globalFont = new Font(MainWindow.font.getName(), Font.PLAIN, backToMenu.getFont().getSize());
        backToMenu.setFont(globalFont);

        backToMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] args = {locationPath, tablePath, domain};
                MainWindow.main(args);
                dispose();
            }
        });

        // creates the GUI
        JPanel trainPanel = new JPanel();
        trainPanel.setLayout(new BoxLayout(trainPanel, BoxLayout.X_AXIS));
        trainPanel.setMaximumSize(new Dimension(1000,100));
        trainPanel.add(trainTestLabel);
        trainPanel.add(timesToTrainTestTextField);
        trainPanel.add(trainTestButton);
        trainPanel.add(backToMenu);

        JPanel textAreaPanel = new JPanel();
        textAreaPanel.setLayout(new BoxLayout(textAreaPanel, BoxLayout.Y_AXIS));
        textAreaPanel.add(new JScrollPane(Operator.textPane));

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.add(trainPanel);
        mainPanel.add(textAreaPanel);

        add(mainPanel);
        setSize(1000, 1000);
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    private void initPopupMenu() {
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("**********************************************************");
        System.out.println();
        System.out.println("Right-click at any time to pause/continue/exit program");
        System.out.println();
        System.out.println("**********************************************************");
        popup = new JPopupMenu();
        ActionListener menuListener = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.out.println("\n[" + event.getActionCommand() + "] program.\n");
                switch (event.getActionCommand()) {
                    case "Pause":
                        if (comparisonThread != null) {
                            if (comparisonThread.isAlive()) {
                                pauseItem.setEnabled(false);
                                continueItem.setEnabled(true);
                                backToMenu.setEnabled(true);
                                comparisonThread.suspend();
                            }
                        }
                        break;
                    case "Continue":
                        if (comparisonThread != null) {
                            if (comparisonThread.isAlive()) {
                                pauseItem.setEnabled(true);
                                continueItem.setEnabled(false);
                                backToMenu.setEnabled(false);
                                comparisonThread.resume();
                            }
                        }
                        break;
                    case "Exit":
                        operator.saveOutput();
                        System.exit(0);
                    default:
                        break;
                }
            }
        };
        popup.add(pauseItem = new JMenuItem("Pause", new ImageIcon("1.gif")));
        pauseItem.setHorizontalTextPosition(JMenuItem.RIGHT);
        pauseItem.addActionListener(menuListener);
        pauseItem.setEnabled(false);
        popup.add(continueItem = new JMenuItem("Continue", new ImageIcon("2.gif")));
        continueItem.setHorizontalTextPosition(JMenuItem.RIGHT);
        continueItem.addActionListener(menuListener);
        continueItem.setEnabled(false);
        popup.addSeparator();
        popup.add(exitItem = new JMenuItem("Exit"));
        exitItem.addActionListener(menuListener);

        popup.setLabel("");
        popup.setBorder(new BevelBorder(BevelBorder.RAISED));
        MouseAdapter mouseAdapter = new MousePopupListener();
        Operator.textPane.addMouseListener(mouseAdapter);
    }

    // An inner class to check whether mouse events are the popup trigger
    class MousePopupListener extends MouseAdapter {
        public void mousePressed(MouseEvent e) {
        }

        public void mouseClicked(MouseEvent e) {
        }

        public void mouseReleased(MouseEvent e) {
            if (e.isPopupTrigger()) {
                popup.show(Operator.textPane, e.getX(), e.getY());
            }
        }
    }
}
