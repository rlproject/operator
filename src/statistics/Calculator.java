package statistics;

import java.util.LinkedList;
import java.util.List;

/**
 * Calculator class.
 * Implements several statistics calculation on agent's raw database
 */
public class Calculator {

    /**
     * get learning curve errors (standard deviation)
     * @param curveLists - learning curve
     * @return leaning curve errors
     */
    public static List<Double> getErrors(List<List<Double>> curveLists) {
        int sizeOfSession = curveLists.get(0).size();
        int numOfSessions = curveLists.size();

        List<Double> errors = new LinkedList<>();

        // set learning curve and it's errors (standard deviation)
        List<Double> singleBatchAverages = new LinkedList<>();
        for (int i = 0; i < sizeOfSession; i++) {
            singleBatchAverages.clear();
            for (int j = 0; j < numOfSessions; j++) {
                double avg = curveLists.get(j).get(i);
                singleBatchAverages.add(avg);
            }
            errors.add(getStdDev(singleBatchAverages));
        }
        return errors;
    }

    /**
     * get learning curve by performing average on all session's
     * learning curves
     * @param curveLists - list of all agent's learning curves
     * @return average learning curve
     */
    public static List<Double> getCurve(List<List<Double>> curveLists) {
        int sizeOfSession = curveLists.get(0).size();
        int numOfSessions = curveLists.size();

        List<Double> learningCurve = new LinkedList<>();

        // set learning curve and it's errors (standard deviation)
        for (int i = 0; i < sizeOfSession; i++) {
            double sum = 0;
            for (int j = 0; j < numOfSessions; j++) {
                double avg = curveLists.get(j).get(i);
                sum += avg;
            }
            learningCurve.add(sum / numOfSessions);
        }
        return learningCurve;
    }

    /**
     * get mean of Double-value list
     * @param data - list of Double value
     * @return mean
     */
    public static double getMean(List<Double> data) {
        double sum = 0.0;
        for (double a : data)
            sum += a;
        return sum / data.size();
    }

    /**
     * get variance of given Double-value list
     * @param data - list of Double value
     * @return variance
     */
    public static double getVariance(List<Double> data) {
        double mean = getMean(data);
        double temp = 0;
        for (double a : data)
            temp += (mean - a) * (mean - a);
        return temp / data.size();
    }

    /**
     * get standard deviation of Double-value list
     * @param data - list of Double value
     * @return standard deviation
     */
    public static double getStdDev(List<Double> data) {
        return Math.sqrt(getVariance(data));
    }
}
