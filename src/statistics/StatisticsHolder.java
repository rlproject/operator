package statistics;

import session.SessionProxy;

/**
 * Statistics Holder is a general object to hold
 * all statistics data retreived from different agents
 */
public class StatisticsHolder {
    private int numOfSessions;
    private int sizeOfSession;

    private DataHolder regAgent;
    private DataHolder simAgent;

    public StatisticsHolder() {}

    /**
     * set agent statistics holder
     * @param holder - statistics holder
     * @param type - agent type
     */
    public void setAgent(DataHolder holder, int type) {
        if (type == SessionProxy.TYPE_REGULAR) {
            regAgent = holder;
        } else if (type == SessionProxy.TYPE_SIMILARITY) {
            simAgent = holder;
        }
    }

    /**
     * get agent's statistics by type
     * @param type - type of agent
     * @return agent's statistics
     */
    public DataHolder getAgent(int type) {
        if (type == SessionProxy.TYPE_REGULAR) {
            return regAgent;
        } else if (type == SessionProxy.TYPE_SIMILARITY) {
            return simAgent;
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "Regular Agent:\n" +
                regAgent.toString() +
                "Similarities Agent:\n" +
                simAgent.toString();
    }
}

