package statistics;

import java.util.LinkedList;
import java.util.List;

/**
 * Data Base to hold all agent's statistics.
 */
public class DataHolder {
    public static final String MOVES = "Moves";
    List<Double> avgMovesCurve;
    List<List<Double>> avgMovesCurveLists;
    List<Double> avgMovesCurvesErrors = new LinkedList<>();
    Double avgMovesUpperBound;
    Double avgMovesLowerBound;

    public static final String KILLS = "Kills";
    List<Double> avgKillsCurve;
    List<List<Double>> avgKillsCurveLists;
    List<Double> avgKillsCurvesErrors;
    Double avgKillsUpperBound;
    Double avgKillsLowerBound;

    public static final String PROGRESSION = "Progression";
    List<Double> avgProgressionCurve;
    List<List<Double>> avgProgressionCurveLists;
    List<Double> avgProgressionCurvesErrors;
    Double avgProgressionUpperBound;
    Double avgProgressionLowerBound;

    public static final String WIN = "Wins";
    List<Double> avgWinCurve;
    List<List<Double>> avgWinCurveLists;
    List<Double> avgWinCurvesErrors;
    Double avgWinUpperBound;
    Double avgWinLowerBound;

    public DataHolder() {
    }

    public void setCurveList(String key, List<List<Double>> value) {
        if (key.equals(MOVES)) {
            avgMovesCurveLists = value;
        } else if (key.equals(KILLS)) {
            avgKillsCurveLists = value;
        } else if (key.equals(PROGRESSION)) {
            avgProgressionCurveLists = value;
        } else if (key.equals(WIN)) {
            avgWinCurveLists = value;
        }
    }

    public List<List<Double>> getCurveList(String key) {
        if (key.equals(MOVES)) {
            return avgMovesCurveLists;
        } else if (key.equals(KILLS)) {
            return avgKillsCurveLists;
        } else if (key.equals(PROGRESSION)) {
            return avgProgressionCurveLists;
        } else if (key.equals(WIN)) {
            return avgWinCurveLists;
        } else {
            return null;
        }
    }

    public void setCurve(String key, List<Double> value) {
        if (key.equals(MOVES)) {
            avgMovesCurve = value;
        } else if (key.equals(KILLS)) {
            avgKillsCurve = value;
        } else if (key.equals(PROGRESSION)) {
            avgProgressionCurve = value;
        } else if (key.equals(WIN)) {
            avgWinCurve = value;
        }
    }

    public List<Double> getCurve(String key) {
        if (key.equals(MOVES)) {
            return avgMovesCurve;
        } else if (key.equals(KILLS)) {
            return avgKillsCurve;
        } else if (key.equals(PROGRESSION)) {
            return avgProgressionCurve;
        } else if (key.equals(WIN)) {
            return avgWinCurve;
        } else {
            return null;
        }
    }

    public void setError(String key, List<Double> value) {
        if (key.equals(MOVES)) {
            avgMovesCurvesErrors = value;
        } else if (key.equals(KILLS)) {
            avgKillsCurvesErrors = value;
        } else if (key.equals(PROGRESSION)) {
            avgProgressionCurvesErrors = value;
        } else if (key.equals(WIN)) {
            avgWinCurvesErrors = value;
        }
    }

    public List<Double> getError(String key) {
        if (key.equals(MOVES)) {
            return avgMovesCurvesErrors;
        } else if (key.equals(KILLS)) {
            return avgKillsCurvesErrors;
        } else if (key.equals(PROGRESSION)) {
            return avgProgressionCurvesErrors;
        } else if (key.equals(WIN)) {
            return avgWinCurvesErrors;
        } else {
            return null;
        }
    }

    public void setUpperBound(String key, Object value) {
        if (key.equals(MOVES)) {
            avgMovesUpperBound = (Double) value;
        } else if (key.equals(KILLS)) {
            avgKillsUpperBound = (Double) value;
        } else if (key.equals(PROGRESSION)) {
            avgProgressionUpperBound = (Double) value;
        } else if (key.equals(WIN)) {
            avgWinUpperBound = (Double) value;
        }
    }

    public void setLowerBound(String key, Object value) {
        if (key.equals(MOVES)) {
            avgMovesLowerBound = (Double) value;
        } else if (key.equals(KILLS)) {
            avgKillsLowerBound = (Double) value;
        } else if (key.equals(PROGRESSION)) {
            avgProgressionLowerBound = (Double) value;
        } else if (key.equals(WIN)) {
            avgWinLowerBound = (Double) value;
        }
    }

    public Double getUpperBound(String key) {
        if (key.equals(MOVES)) {
            return avgMovesUpperBound;
        } else if (key.equals(KILLS)) {
            return avgKillsUpperBound;
        } else if (key.equals(PROGRESSION)) {
            return avgProgressionUpperBound;
        } else if (key.equals(WIN)) {
            return avgWinUpperBound;
        } else {
            return null;
        }
    }

    public Double getLowerBound(String key) {
        if (key.equals(MOVES)) {
            return avgMovesLowerBound;
        } else if (key.equals(KILLS)) {
            return avgKillsLowerBound;
        } else if (key.equals(PROGRESSION)) {
            return avgProgressionLowerBound;
        } else if (key.equals(WIN)) {
            return avgWinLowerBound;
        } else {
            return null;
        }
    }
}
