package session;

import statistics.Calculator;
import statistics.DataHolder;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * ChaseSessionRunner:
 * Loads the Game class (by path) and runs it.
 * <p>
 * Implements SessionProxy Interface that support appropriate actions
 * to handle all the game actions (train, test, get results, etc.)
 */
public class ChaseSessionRunner extends SessionRunner {
    //statics attributes to handle the game process:
    private static final int SIZE = 7;
    //board width size
    private static final int WIDTH = SIZE;
    //board height size
    private static final int HEIGHT = SIZE;

    private int NUM_OF_TRAINS;
    private int NUM_OF_TESTS;
    private int NUM_OF_BATCHES;

    /**
     * Constructor
     *
     * @param pathToClass - path of a SessionProxy class
     */
    public ChaseSessionRunner(String pathToClass, String pathToTable) {
        super(pathToClass, pathToTable);
    }

    /**
     * Run the game and extract statistics at the end:
     * the game runs by this fashion:
     * 1) Create a game session instance
     * 2) Create game table and initial conditions
     * 3) Set agent type to run
     * 4) Run X Sessions, and in each session:
     * - get session upper/lower bound
     * - get session learning curve data
     *
     * @param agentType - age
     * @return statistics - statistics of the sessions
     */
    public DataHolder runWithStatistics(int agentType,
                                        int numOfTrains,
                                        int numOfTests,
                                        int numOfBacthes) {
        NUM_OF_TRAINS = numOfTrains;
        NUM_OF_TESTS = numOfTests;
        NUM_OF_BATCHES = numOfBacthes;
        //data structures to hold sessions results (statistics)
        List<List<Double>> learningCurveList = new LinkedList<>();
        List<Double> upperBoundResults = new LinkedList<>();
        List<Double> lowerBoundResults = new LinkedList<>();
        //set number of sessions to run
        int numOfSessions = DEFAULT_NUM_OF_SESSIONS;

        //create a session instance
        try {
            session = constructor.newInstance();
        } catch (Exception e) {
            System.err.println("Error occurred while invoking session's constructor:");
            System.err.println(e.getMessage());
            System.exit(-1);
        }

        //create game and initial conditions
        create(WIDTH, HEIGHT);
        //set agent type
        setAgentType(agentType);

        //run the sessions
        for (int i = 1; i <= numOfSessions; i++) {
            System.out.println("----- Running session number " + i + " -----");

            //run session
            SingleSessionDataHolder sessionResultsHolder = runSession();

            if (sessionResultsHolder != null) {
                //update upper/lower bounds
                upperBoundResults.add(sessionResultsHolder.getUpperBound(SingleSessionDataHolder.MOVES));
                lowerBoundResults.add(sessionResultsHolder.getLowerBound(SingleSessionDataHolder.MOVES));
                learningCurveList.add(sessionResultsHolder.getCurve(SingleSessionDataHolder.MOVES));
            } else {
                return null;
            }
            System.out.println();
        }

        //create data structure to hold all statistics
        DataHolder holder = new DataHolder();

        holder.setCurveList(DataHolder.MOVES, learningCurveList);
        holder.setCurve(DataHolder.MOVES, Calculator.getCurve(learningCurveList));
        holder.setError(DataHolder.MOVES, Calculator.getErrors(learningCurveList));
        holder.setUpperBound(DataHolder.MOVES, Collections.max(upperBoundResults));
        holder.setLowerBound(DataHolder.MOVES, Collections.min(lowerBoundResults));


        //return final results
        return holder;
    }

    /**
     * Run a game session and return results.
     * Each session contains of batches of trainings and a final batch of tests
     *
     * @return results - data structure containing move counts and  upper and lower bounds
     */
    private SingleSessionDataHolder runSession() {
        //data structure to hold session average moves count
        List<Double> avgMovesCount = new LinkedList<>();
        //set the session size: the size is the number of games to run in one batch
        int batchSize = NUM_OF_TRAINS;
        //set num of batches
        int numOfBatches = NUM_OF_BATCHES;
        //set total trains number
        int totalRuns = batchSize * numOfBatches;
        double sum = 0;

        //create agent
        create(WIDTH, HEIGHT);

        //perform all trains
        for (int i = 1; i <= totalRuns; i++) {
            //run one game
            run();

            // show average move count after each batch
            if (i % batchSize == 0) {
                //extract average moves count
                double avg = sum / batchSize;
                //add to list
                avgMovesCount.add(avg);
                System.out.println("Avg moves: " + avg);
                sum = 0;
            } else {
                int movesCount = getMovesCount();

                //add current session's move count to total sum
                sum += movesCount;
            }
        }

        //create data structure to hold the session result
        SingleSessionDataHolder holder = new SingleSessionDataHolder();
        holder.setCurve(SingleSessionDataHolder.MOVES, avgMovesCount);
        holder.setUpperBound(SingleSessionDataHolder.MOVES, Collections.max(avgMovesCount));
        holder.setLowerBound(SingleSessionDataHolder.MOVES, Collections.min(avgMovesCount));

        return holder;
    }
}
