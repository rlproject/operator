package session;

import statistics.Calculator;
import statistics.DataHolder;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * MatioSessionRunner:
 * Loads the Game class (by path) and runs it.
 * <p>
 * Implements SessionProxy Interface that support appropriate actions
 * to handle all the game actions (train, test, get results, etc.)
 */
public class MarioSessionRunner extends SessionRunner {

    /**
     * Constructor
     * @param pathToClass - path of a SessionProxy class
     */
    public MarioSessionRunner(String pathToClass, String pathToTable) {
        super(pathToClass, pathToTable);
    }

    @Override
    public DataHolder runWithStatistics(int agentType,
                                        int numOfTrains,
                                        int numOfTests,
                                        int numOfBatches) {
        final int TRAIN_TOTAL = numOfTrains;
        //data structures to hold sessions results (statistics)

        //kills
        List<List<Double>> killsCurveList = new LinkedList<>();
        List<Double> killsUpperBoundList = new LinkedList<>();
        List<Double> killsLowerBoundList = new LinkedList<>();

        //progression
        List<List<Double>> progressionCurveList = new LinkedList<>();
        List<Double> progressionUpperBoundList = new LinkedList<>();
        List<Double> progressionLowerBoundList = new LinkedList<>();

        //wins
        List<List<Double>> winsCurveList = new LinkedList<>();
        List<Double> winsUpperBoundList = new LinkedList<>();
        List<Double> winsLowerBoundList = new LinkedList<>();

        //set number of sessions to run
        int numOfSessions = DEFAULT_NUM_OF_SESSIONS;

        //create a session instance
        try {
            session = constructor.newInstance();
        } catch (Exception e) {
            System.err.println("Error occurred while invoking session's constructor:");
            System.err.println(e.getMessage());
            System.exit(-1);
        }

        //create game and initial conditions
        create(0, 0);
        //set agent type
        setAgentType(agentType);

        //run the sessions
        for (int i = 1; i <= numOfSessions; i++) {
            System.out.println("----- Running session number " + i + " -----\n");
            //train
            System.out.println("------- Start Training -------");
            train(TRAIN_TOTAL / numOfSessions);
            System.out.println("-------- End Training --------\n");

            //run session
            System.out.println("------- Start  testing -------");
            SingleSessionDataHolder testResultsHolder = test(numOfTests, numOfBatches);
            System.out.println("-------- End  testing --------");

            if (testResultsHolder != null) {
                //update kills statistics
                killsUpperBoundList.add(testResultsHolder.getUpperBound(SingleSessionDataHolder.KILLS));
                killsLowerBoundList.add(testResultsHolder.getLowerBound(SingleSessionDataHolder.KILLS));
                killsCurveList.add(testResultsHolder.getCurve(SingleSessionDataHolder.KILLS));

                //update progression statistics
                progressionUpperBoundList.add(testResultsHolder.getUpperBound(SingleSessionDataHolder.PROGRESSION));
                progressionLowerBoundList.add(testResultsHolder.getLowerBound(SingleSessionDataHolder.PROGRESSION));
                progressionCurveList.add(testResultsHolder.getCurve(SingleSessionDataHolder.PROGRESSION));

                //update wins statistics
                winsUpperBoundList.add(testResultsHolder.getUpperBound(SingleSessionDataHolder.WIN));
                winsLowerBoundList.add(testResultsHolder.getLowerBound(SingleSessionDataHolder.WIN));
                winsCurveList.add(testResultsHolder.getCurve(SingleSessionDataHolder.WIN));
            } else {
                return null;
            }
            System.out.println();
        }

        //create data structure to hold all statistics
        DataHolder holder = new DataHolder();

        //kills
        holder.setCurveList(DataHolder.KILLS, killsCurveList);
        holder.setCurve(DataHolder.KILLS, Calculator.getCurve(killsCurveList));
        holder.setError(DataHolder.KILLS, Calculator.getErrors(killsCurveList));
        holder.setUpperBound(DataHolder.KILLS, Collections.max(killsUpperBoundList));
        holder.setLowerBound(DataHolder.KILLS, Collections.min(killsLowerBoundList));

        //progression
        holder.setCurveList(DataHolder.PROGRESSION, progressionCurveList);
        holder.setCurve(DataHolder.PROGRESSION, Calculator.getCurve(progressionCurveList));
        holder.setError(DataHolder.PROGRESSION, Calculator.getErrors(progressionCurveList));
        holder.setUpperBound(DataHolder.PROGRESSION, Collections.max(progressionUpperBoundList));
        holder.setLowerBound(DataHolder.PROGRESSION, Collections.min(progressionLowerBoundList));

        //wins
        holder.setCurveList(DataHolder.WIN, winsCurveList);
        holder.setCurve(DataHolder.WIN, Calculator.getCurve(winsCurveList));
        holder.setError(DataHolder.WIN, Calculator.getErrors(winsCurveList));
        holder.setUpperBound(DataHolder.WIN, Collections.max(winsUpperBoundList));
        holder.setLowerBound(DataHolder.WIN, Collections.min(winsLowerBoundList));

        //return final results
        return holder;
    }

    public void train(int times) {
        //set agent to train mode
        setMode(SessionProxy.MODE_TRAIN);
        for (int i = 0; i < times; i++) {
            run();
        }
    }

    /**
     * Run a game session and return results.
     * Each session contains of batches of trainings and a final batch of tests
     *
     * @return results - data structure containing move counts and  upper and lower bounds
     */
    public SingleSessionDataHolder test(int times, int batches) {
        //data structure to hold session average moves count
        List<Double> avgKillsCountCurve = new LinkedList<>();
        List<Double> avgProgressionCurve = new LinkedList<>();
        List<Double> avgWinsCountCurve = new LinkedList<>();
        //set the session size: the size is the number games to run in one batch
        int batchSize = times;
        int numOfBatches = batches;
        //set total runs number
        int totalRuns = batchSize * numOfBatches;
        double killsSum = 0;
        double progressionSum = 0;
        double winsSum = 0;

        //set agent to test mode
        setMode(SessionProxy.MODE_TEST);

        //perform all trains
        for (int i = 1; i <= totalRuns; i++) {
            //run one game
            run();

            // show average move count after each batch
            if (i % batchSize == 0) {
                //extract average moves count
                double avgKills         = killsSum / batchSize;
                double avgProgression   = progressionSum / batchSize;
                double avgWins          = winsSum / batchSize;

                //add to list
                avgKillsCountCurve.add(avgKills);
                avgProgressionCurve.add(avgProgression);
                avgWinsCountCurve.add(avgWins);

                System.out.println(String.format("Avg kills: %.2f\tAvg Progress: %.2f\tTotal wins: %.2f",
                        avgKills, avgProgression, avgWins));


                killsSum        = 0;
                progressionSum  = 0;
                winsSum         = 0;

            } else {
                int killCount   = getKillsCount();
                int progression = getProgression();
                int winCount    = getWinCount();

                //add current session's move count to total sum
                killsSum        += killCount;
                progressionSum  += progression;
                winsSum         += winCount;
            }
        }

        //create data structure to hold the session result
        SingleSessionDataHolder holder = new SingleSessionDataHolder();

        //kills statistics
        holder.setCurve(SingleSessionDataHolder.KILLS, avgKillsCountCurve);
        holder.setUpperBound(SingleSessionDataHolder.KILLS, Collections.max(avgKillsCountCurve));
        holder.setLowerBound(SingleSessionDataHolder.KILLS, Collections.min(avgKillsCountCurve));

        //progression statistics
        holder.setCurve(SingleSessionDataHolder.PROGRESSION, avgProgressionCurve);
        holder.setUpperBound(SingleSessionDataHolder.PROGRESSION, Collections.max(avgProgressionCurve));
        holder.setLowerBound(SingleSessionDataHolder.PROGRESSION, Collections.min(avgProgressionCurve));

        //wins statistics
        holder.setCurve(SingleSessionDataHolder.WIN, avgWinsCountCurve);
        holder.setUpperBound(SingleSessionDataHolder.WIN, Collections.max(avgWinsCountCurve));
        holder.setLowerBound(SingleSessionDataHolder.WIN, Collections.min(avgWinsCountCurve));

        return holder;
    }

    @Override
    public int getProgression() {
        try {
            return (int) getProgression.invoke(session);
        } catch (Exception e) {
            System.err.println("Fatal error occurred while invoking \"getProgression()\" method:");
            System.err.println(e.getMessage());
            e.printStackTrace();
            System.exit(-1);
        }
        return -1;
    }

    @Override
    public int getKillsCount() {
        try {
            return (int) getKillsCount.invoke(session);
        } catch (Exception e) {
            System.err.println("Fatal error occurred while invoking \"getKillsCount()\" method:");
            System.err.println(e.getMessage());
            e.printStackTrace();
            System.exit(-1);
        }
        return -1;
    }

    @Override
    public int getWinCount() {
        try {
            return (int) getWinCount.invoke(session);
        } catch (Exception e) {
            System.err.println("Fatal error occurred while invoking \"getWinCount()\" method:");
            System.err.println(e.getMessage());
            e.printStackTrace();
            System.exit(-1);
        }
        return -1;
    }
}
