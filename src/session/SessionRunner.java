package session;

import statistics.DataHolder;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

/**
 * SessionRunner abstract class.
 * perform all tasks necessary to run any domain session:
 * - load sessionProxy class
 * - copy similarities table to current working directory
 * -run sessionProxy methods
 */
public abstract class SessionRunner extends ClassLoader implements SessionProxy {
    //default number of game sessions to run
    public static final int DEFAULT_NUM_OF_SESSIONS = 10;
    //default size of game session
    public static final int DEFAULT_SIZE_OF_BATCH = 1000;
    protected static final String DEFAULT_SESSION_NAME = "Session";
    //path to distant project, containing the sessionProxy class to be loaded
    protected final String path;
    protected final String tablePath;

    protected Object session;
    protected Constructor<?> constructor;
    protected Method create;
    protected Method run;
    protected Method getMovesCount;
    protected Method setMode;
    protected Method setAgentType;
    protected Method getProgression;
    protected Method getWinCount;
    protected Method getKillsCount;

    public abstract DataHolder runWithStatistics(int agentType,
                                                 int numOfTrains,
                                                 int numOfTests,
                                                 int numOfBacthes);

    /**
     * Constructor
     * @param pathToClass - path of a SessionProxy class
     */
    public SessionRunner(String pathToClass, String pathToTable) {
        path = pathToClass;
        tablePath = pathToTable;
        loadSessionRunner(path);
    }

    /**
     * loads the actual SessionProxy class
     * @param path - path to the class
     */
    protected void loadSessionRunner(String path) {
        try {
            // Create a new ClassLoader
            URLClassLoader sessionClassLoader = getLoader(path);

            //before loading the class, copy necessary data from the class's location
            copyData();

            //get the actual Session-class exact location
            String actualClassPath = getClassName(DEFAULT_SESSION_NAME, new File(path).listFiles(), true);

            // Load the Session class using its binary name
            Class<?> sessionClass = sessionClassLoader.loadClass(actualClassPath);

            /**
             * test!!
             */

//            Class<?>[] interfaces = sessionClass.getInterfaces();
//            for (Class<?> inter : interfaces) {
//                boolean b = inter.getSimpleName().equals(SessionProxy.class.getSimpleName());
//                System.out.println(b);
//            }

            /**
             * test!!
             */

            System.out.println("Loaded class name: " + sessionClass.getCanonicalName());

            // Get all session-class methods
            constructor = sessionClass.getConstructor();
            create = sessionClass.getMethod("create", int.class, int.class);
            run = sessionClass.getMethod("run");
            getMovesCount = sessionClass.getMethod("getMovesCount");
            setMode = sessionClass.getMethod("setMode", int.class);
            setAgentType = sessionClass.getMethod("setAgentType", int.class);
            getProgression = sessionClass.getMethod("getProgression");
            getWinCount = sessionClass.getMethod("getWinCount");
            getKillsCount = sessionClass.getMethod("getKillsCount");

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * get the actual class game.
     * restrictions:
     * 1. must have one occurrence of 'className' only
     *
     * @param className          - Session class name
     * @param baseDirectory      - project directory file
     * @param isInDefaultPackage - determine if current location is the base directory
     * @return actual class name to be reloaded by the class loader
     */
    private String getClassName(String className, File[] baseDirectory, boolean isInDefaultPackage) {
        //iterate (recursively) over the base folder in which the session class resides
        for (File file : baseDirectory) {
            //if the current file is not a class
            if (file.isDirectory()) {
                //call again to to this method with the current dir as the base directory
                String result = getClassName(className, file.listFiles(), false);
                if (result != null) {
                    //return class name
                    return result;
                }
                //if the current file is a class
            } else if (file.getName().endsWith(className + ".class")) {
                //if it's in the default base directory
                if (isInDefaultPackage) {
                    //return class name only
                    return className;
                }
                //if the desired class isn't in the base directory
                String packageName = file.getParent();
                String pathTo = packageName.replace(path + "\\", "") + "\\" + className;
                return pathTo.replace("\\", ".");
            }
        }
        return null;
    }

    /**
     * Before loading the session class,
     * copy any necessary data from loaded-class classPath (location)
     * in order for the loaded class to function properly.
     */
    protected void copyData() {
        try {
            //create a new empty file to store copied data in: "table.ser"
            File newFile = new File(System.getProperty("user.dir") + "\\table.ser");
            if (newFile.createNewFile()) {
                System.out.println("File is created!");
            } else {
                System.out.println("File is already existed!");
            }


            //get loaded-class "table.ser"'s path
            Path from = Paths.get(tablePath);
            //get newFile's path
            Path to = Paths.get(newFile.getAbsolutePath());

            //copy the file
            System.out.println("------- Copying  table -------");
            Files.copy(from, to, StandardCopyOption.REPLACE_EXISTING);
            System.out.println("---- successfully  Copied ----");

        } catch (IOException e) {
            System.err.println("copyData(): Error occurred:");
            System.err.println(e.getMessage());
            System.exit(-1);
        }
    }

    /**
     * Get a path to the desired session-class and
     * return its class loader
     *
     * @param path - path to the class
     * @return class loader
     * @throws Exception
     */
    protected URLClassLoader getLoader(String path) throws Exception {
        //translate path to URL
        File file = new File(path);

        //convert to URL in order to create URL ClassLoader
        URL[] urlPath = new URL[]{file.toURI().toURL()};

        //return relevant class loader
        return new URLClassLoader(urlPath);
    }

    //---------------Implemented Methods---------------//
    @Override
    public void create(int width, int height) {
        try {
            create.invoke(session, width, height);
        } catch (Exception e) {
            System.err.println("Fatal error occurred while invoking \"create()\" method:");
            System.err.println(e.getMessage());
            System.exit(-1);
        }
    }

    @Override
    public void run() {
        try {
            run.invoke(session);
        } catch (Exception e) {
            System.err.println("Fatal error occurred while invoking \"run()\" method:");
            System.err.println(e.getMessage());
            e.printStackTrace();
            System.exit(-1);
        }
    }

    @Override
    public int getMovesCount() {
        try {
            return (int) getMovesCount.invoke(session);
        } catch (Exception e) {
            System.err.println("Fatal error occurred while invoking \"getMovesCount()\" method:");
            System.err.println(e.getMessage());
            System.exit(-1);
        }
        return -1;
    }

    @Override
    public void setMode(int agentMode) {
        try {
            setMode.invoke(session, agentMode);
        } catch (Exception e) {
            System.err.println("Fatal error occurred while invoking \"setMode()\" method:");
            System.err.println(e.getMessage());
            System.exit(-1);
        }
    }

    @Override
    public void setAgentType(int agentType) {
        try {
            setAgentType.invoke(session, agentType);
        } catch (Exception e) {
            System.err.println("Fatal error occurred while invoking \"setAgentType()\" method:");
            System.err.println(e.getMessage());
            System.exit(-1);
        }
    }

    @Override
    public int getProgression() {
        return 0;
    }

    @Override
    public int getWinCount() {
        return 0;
    }

    @Override
    public int getKillsCount() {
        return 0;
    }

    @Override
    public void setDrawable(boolean isDrawable) {}
    //---------------End of Implemented Methods---------------//

    /**
     * Inner class that represent single-session statistics holder
     */
    protected class SingleSessionDataHolder {
        public static final String MOVES = "avgMoves";
        List<Double> avgMovesCurve;
        Double avgMovesUpperBound;
        Double avgMovesLowerBound;

        public static final String KILLS = "avgKills";
        List<Double> avgKillsCurve;
        Double avgKillsUpperBound;
        Double avgKillsLowerBound;

        public static final String PROGRESSION = "avgProgression";
        List<Double> avgProgressionCurve;
        Double avgProgressionUpperBound;
        Double avgProgressionLowerBound;

        public static final String WIN = "avgWin";
        List<Double> avgWinCurve;
        Double avgWinUpperBound;
        Double avgWinLowerBound;

        public SingleSessionDataHolder() {
        }

        public void setCurve(String key, List<Double> value) {
            if (key.equals(MOVES)) {
                avgMovesCurve = value;
            } else if (key.equals(KILLS)) {
                avgKillsCurve = value;
            } else if (key.equals(PROGRESSION)) {
                avgProgressionCurve = value;
            } else if (key.equals(WIN)) {
                avgWinCurve = value;
            }
        }

        public List<Double> getCurve(String key) {
            if (key.equals(MOVES)) {
                return avgMovesCurve;
            } else if (key.equals(KILLS)) {
                return avgKillsCurve;
            } else if (key.equals(PROGRESSION)) {
                return avgProgressionCurve;
            } else if (key.equals(WIN)) {
                return avgWinCurve;
            } else {
                return null;
            }
        }

        public void setUpperBound(String key, Double value) {
            if (key.equals(MOVES)) {
                avgMovesUpperBound = value;
            } else if (key.equals(KILLS)) {
                avgKillsUpperBound = value;
            } else if (key.equals(PROGRESSION)) {
                avgProgressionUpperBound = value;
            } else if (key.equals(WIN)) {
                avgWinUpperBound = value;
            }
        }

        public Double getUpperBound(String key) {
            if (key.equals(MOVES)) {
                return avgMovesUpperBound;
            } else if (key.equals(KILLS)) {
                return avgKillsUpperBound;
            } else if (key.equals(PROGRESSION)) {
                return avgProgressionUpperBound;
            } else if (key.equals(WIN)) {
                return avgWinUpperBound;
            } else {
                return null;
            }
        }

        public void setLowerBound(String key, Double value) {
            if (key.equals(MOVES)) {
                avgMovesLowerBound = value;
            } else if (key.equals(KILLS)) {
                avgKillsLowerBound = value;
            } else if (key.equals(PROGRESSION)) {
                avgProgressionLowerBound = value;
            } else if (key.equals(WIN)) {
                avgWinLowerBound = value;
            }
        }

        public Double getLowerBound(String key) {
            if (key.equals(MOVES)) {
                return avgMovesLowerBound;
            } else if (key.equals(KILLS)) {
                return avgKillsLowerBound;
            } else if (key.equals(PROGRESSION)) {
                return avgProgressionLowerBound;
            } else if (key.equals(WIN)) {
                return avgWinLowerBound;
            } else {
                return null;
            }
        }
    }
}
