package session;

import statistics.StatisticsHolder;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

/**
 * ChaseSessionRunner:
 * Loads the Game class (by path) and runs it.
 * <p>
 * Implements SessionProxy Interface that support appropriate actions
 * to handle all the game actions (train, test, get results, etc.)
 */
public class ChaseSessionRunner extends ClassLoader implements SessionProxy {
    private static final String DEFAULT_SESSION_NAME = "Session";
    //statics attributes to handle the game process:
    private static final int SIZE = 7;
    //board width size
    private static final int WIDTH = SIZE;
    //board height size
    private static final int HEIGHT = SIZE;

    //default number of game sessions to run
    private static final int DEFAULT_NUM_OF_BATCHES = 10;
    //default size of game session
    private static final int DEFAULT_SIZE_OF_BATCH = 1000;

    //path to distant project, containing the sessionProxy class to be loaded
    private final String path;

    /**
     * Java Reflection objects:
     * represent the 'session proxy' using reflection:
     * class, constructor and methods.
     */
    private Object session;
    private Constructor<?> constructor;
    private Method create;
    private Method run;
    private Method getMovesCount;
    private Method setMode;
    private Method setAgentType;

    /**
     * Constructor
     *
     * @param pathToClass - path of a SessionProxy class
     */
    public ChaseSessionRunner(String pathToClass) {
        path = pathToClass;
        //load the session class
        loadSessionRunner(path);
    }

    /**
     * Get a path to the desired session-class and
     * return its class loader
     *
     * @param path - path to the class
     * @return class loader
     * @throws Exception
     */
    private URLClassLoader getLoader(String path) throws Exception {
        //translate path to URL
        File file = new File(path);

        //convert to URL in order to create URL ClassLoader
        URL[] urlPath = new URL[]{file.toURI().toURL()};

        //return relevant class loader
        return new URLClassLoader(urlPath);
    }

    /**
     * get the actual class game.
     * restrictions:
     * 1. must have one occurrence of 'className' only
     *
     * @param className          - Session class name
     * @param baseDirectory      - project directory file
     * @param isInDefaultPackage - determine if current location is the base directory
     * @return actual class name to be reloaded by the class loader
     */
    private String getClassName(String className, File[] baseDirectory, boolean isInDefaultPackage) {
        //iterate (recursively) over the base folder in which the session class resides
        for (File file : baseDirectory) {
            //if the current file is not a class
            if (!file.getName().endsWith(".class")) {
                //call again to to this method with the current dir as the base directory
                String result = getClassName(className, file.listFiles(), false);
                if (result != null) {
                    //return class name
                    return result;
                }
                //if the current file is a class
            } else if (file.getName().endsWith(className + ".class")) {
                //if it's in the default base directory
                if (isInDefaultPackage) {
                    //return class name only
                    return className;
                }
                //if the desired class isn't in the base directory
                String packageName = file.getParent();
                //create the class name by it's relative path
                return packageName.substring(packageName.lastIndexOf("\\") + 1, packageName.length())
                        + "."
                        + className;
            }
        }
        return null;
    }

    /**
     * loads the actual SessionProxy class
     *
     * @param path - path to the class
     */
    private void loadSessionRunner(String path) {
        try {
            // Create a new ClassLoader
            URLClassLoader sessionClassLoader = getLoader(path);

            //before loading the class, copy necessary data from the class's location
            copyData();

            //get the actual Session-class exact location
            String actualClassPath = getClassName(DEFAULT_SESSION_NAME, new File(path).listFiles(), true);

            // Load the Session class using its binary name
            Class<?> sessionClass = sessionClassLoader.loadClass(actualClassPath);

            System.out.println("Loaded class name: " + sessionClass.getCanonicalName());

            // Get all session-class methods
            constructor = sessionClass.getConstructor();
            create = sessionClass.getMethod("create", int.class, int.class);
            run = sessionClass.getMethod("run");
            getMovesCount = sessionClass.getMethod("getMovesCount");
            setMode = sessionClass.getMethod("setMode", int.class);
            setAgentType = sessionClass.getMethod("setAgentType", int.class);

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Before loading the session class,
     * copy any necessary data from loaded-class classPath (location)
     * in order for the loaded class to function properly.
     */
    private void copyData() {
        try {
            //create a new empty file to store copied data in: "table.ser"
            File newFile = new File(System.getProperty("user.dir") + "\\table.ser");
            if (newFile.createNewFile()) {
                System.out.println("File is created!");
            } else {
                System.out.println("File is already existed!");
            }


            //get loaded-class "table.ser"'s path
            Path from = Paths.get(path.substring(0, path.indexOf("out")) + "table.ser");

            //get newFile's path
            Path to = Paths.get(newFile.getAbsolutePath());

            //copy the file
            System.out.println("------- Copying  table -------");
            Files.copy(from, to, StandardCopyOption.REPLACE_EXISTING);
            System.out.println("---- successfully  Copied ----");

        } catch (IOException e) {
            System.err.println("copyData(): Error occurred:");
            System.err.println(e.getMessage());
            System.exit(-1);
        }
    }

    /**
     * Run the game and extract statistics at the end:
     * the game runs by this fashion:
     * 1) Create a game session instance
     * 2) Create game table and initial conditions
     * 3) Set agent type to run
     * 4) Run X Sessions, and in each session:
     * - get session upper/lower bound
     * - get session learning curve data
     *
     * @param agentType - age
     * @return statistics - statistics of the sessions
     */
    public Map<String, Object> runWithStatistics(int agentType) {
        //data structures to hold sessions results (statistics)
        List<List<Double>> learningCurveList = new LinkedList<>();
        List<Double> upperBoundResults = new LinkedList<>();
        List<Double> lowerBoundResults = new LinkedList<>();
        //set number of sessions to run
        int numOfSessions = DEFAULT_NUM_OF_BATCHES;

        //create a session instance
        try {
            session = constructor.newInstance();
        } catch (Exception e) {
            System.err.println("Error occurred while invoking session's constructor:");
            System.err.println(e.getMessage());
            System.exit(-1);
        }

        //create game and initial conditions
        create(WIDTH, HEIGHT);
        //set agent type
        setAgentType(agentType);

        //run the sessions
        for (int i = 1; i <= numOfSessions; i++) {
            System.out.println("----- Running session number " + i + " -----");

            //run session
            Map<String, Object> sessionResult = runSession();

            if (sessionResult != null) {
                //update upper/lower bounds
                lowerBoundResults.add((double) sessionResult.get(StatisticsHolder.LOWER_BOUND_DATA));
                upperBoundResults.add((double) sessionResult.get(StatisticsHolder.UPPER_BOUND_DATA));

                //add current session statistics to learning curve
                if (sessionResult.get(StatisticsHolder.LEARNING_CURVE_DATA) instanceof List) {
                    @SuppressWarnings("unchecked")
                    List<Double> sessionLearningCurve = (List<Double>) sessionResult.get(StatisticsHolder.LEARNING_CURVE_DATA);
                    if (sessionLearningCurve != null) {
                        learningCurveList.add(sessionLearningCurve);
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                return null;
            }
            System.out.println();
        }

        //create data structure to hold all statistics
        Map<String, Object> statistics = new LinkedHashMap<>();

        statistics.put(StatisticsHolder.LEARNING_CURVE_DATA_LIST, learningCurveList);
        statistics.put(StatisticsHolder.UPPER_BOUND_DATA, Collections.max(upperBoundResults));
        statistics.put(StatisticsHolder.LOWER_BOUND_DATA, Collections.min(lowerBoundResults));

        //return final results
        return statistics;
    }

    /**
     * Run a game session and return results.
     * Each session contains of batches of trainings and a final batch of tests
     *
     * @return results - data structure containing move counts and  upper and lower bounds
     */
    private Map<String, Object> runSession() {
        //data structure to hold session average moves count
        List<Double> avgMovesCount = new LinkedList<>();
        //set the session size: the size is the number games to run in one batch
        int batchSize = DEFAULT_SIZE_OF_BATCH;
        //set num of batches
        int numOfBatches = DEFAULT_NUM_OF_BATCHES;
        //set total trains number
        int totalTrains = batchSize * numOfBatches;
        double sum = 0;

        //create agent
        create(WIDTH, HEIGHT);

        //begin training
        System.out.println("Begin training");

        //perform all trains
        for (int i = 1; i <= totalTrains; i++) {
            //run one game
            run();

            // show average move count after each batch
            if (i % batchSize == 0) {
                //extract average moves count
                double avg = sum / batchSize;
                //add to list
                avgMovesCount.add(avg);
                System.out.println("Avg moves: " + avg);
                sum = 0;
            } else {
                int movesCount = getMovesCount();

                //add current session's move count to total sum
                sum += movesCount;
            }
        }

        //create data structure to hold the session result
        Map<String, Object> results = new LinkedHashMap<>();

        results.put(StatisticsHolder.LEARNING_CURVE_DATA, avgMovesCount);
        results.put(StatisticsHolder.UPPER_BOUND_DATA, Collections.max(avgMovesCount));
        results.put(StatisticsHolder.LOWER_BOUND_DATA, Collections.min(avgMovesCount));

        return results;
    }

    //---------------Implemented Methods---------------//

    @Override
    public void create(int width, int height) {
        try {
            create.invoke(session, width, height);
        } catch (Exception e) {
            System.err.println("Fatal error occurred while invoking \"create()\" method:");
            System.err.println(e.getMessage());
            System.exit(-1);
        }
    }

    @Override
    public void run() {
        try {
            run.invoke(session);
        } catch (Exception e) {
            System.err.println("Fatal error occurred while invoking \"run()\" method:");
            System.err.println(e.getMessage());
            System.exit(-1);
        }
    }

    @Override
    public int getMovesCount() {
        try {
            return (int) getMovesCount.invoke(session);
        } catch (Exception e) {
            System.err.println("Fatal error occurred while invoking \"getMovesCount()\" method:");
            System.err.println(e.getMessage());
            System.exit(-1);
        }
        return -1;
    }

    @Override
    public void setMode(int agentMode) {
        try {
            setMode.invoke(session, agentMode);
        } catch (Exception e) {
            System.err.println("Fatal error occurred while invoking \"setMode()\" method:");
            System.err.println(e.getMessage());
            System.exit(-1);
        }
    }

    @Override
    public void setAgentType(int agentType) {
        try {
            setAgentType.invoke(session, agentType);
        } catch (Exception e) {
            System.err.println("Fatal error occurred while invoking \"setAgentType()\" method:");
            System.err.println(e.getMessage());
            System.exit(-1);
        }
    }

    @Override
    public int getProgression() {
        return 0;
    }

    @Override
    public int getWinCount() {
        return 0;
    }

    @Override
    public int getKillsCount() {
        return 0;
    }
    //---------------End of Implemented Methods---------------//
}
